package test;

import chair.core.Diary;
import robot.calibeta.Motion;

/**
 * Un peu tard pour tester Motion comme il convient, mais permet de débusquer un
 * bug ou deux et de généraliser rotateWhenOpposite(false)
 * 
 * @author nomhad
 * 
 */
public class MotionTest {

	/***
	 * Information de debug, histoire de vérifier que tout est bien calculé
	 */
	private static void debug() {
		Diary.logln("Capdeviation : " + Motion.getCapeDeviation()
				+ ", NordDeviation : " + Motion.getNorthDeviation());
	}

	/**
	 * Factorise les test de rotations (ajoute déplacement et pause)
	 * 
	 * @param name
	 *            Explique ce qu'on teste
	 * @param cap
	 *            Le cap à appliquer au robot
	 * @param deviation
	 *            Déviation par rapport au cap pour déplacement
	 */
	private static void taste(String name, int cap, int deviation) {
		Diary.logln("\n" + name);
		Motion.changeCapeAngle(cap);
		Motion.rotateFromCape(deviation);
		Motion.travel(50);
		debug();
		// On nous donne le temps de regarder ce qui se passe
		Motion.haveABreak(2000);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Motion.setRotateShortest(true);

		// Mise en place de shortest
		taste("Nord", 0, 0);
		taste("Nord, 45° droite", 0, -45);
		taste("Sud", 180, 0);

		// Vérification RotateWhenOpposite(false)
		taste("Nord", 0, 0);
		taste("Sud, 45° droite", 180, -45);
		taste("Sud, 90° droite", 180, -90);
		taste("Sud, 45° droite", 180, -45);
		taste("Nord, 45° droite", 0, -45);

		// Vérification côté + désactivation de rotateShortest
		taste("Est, 45° gauche", -90, 45);
		taste("Ouest, 45° gauche", 90, 45);
		Diary.logln("Désactive rotateShortest");
		Motion.setRotateShortest(false);
		Motion.haveABreak(2000);
		taste("Est, 45° gauche", -90, 45);
		taste("Ouest, 45° gauche", 90, 45);

	}

}
