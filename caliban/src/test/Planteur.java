package test;

import java.util.ArrayList;

import chair.core.Diary;


/**
 * Pas la peine de lancer tout le simulateur quand on a cerner le problème vers ArrayList
 */
public class Planteur {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Diary.init();
		ArrayList<Integer> ray = new ArrayList<Integer>();
		// problème linker il semble
		// http://lejos.sourceforge.net/forum/viewtopic.php?t=1809
		//Diary.logln("Ray vide ? " + ray.isEmpty());
		Diary.logln("Ray vide ? " + ray.size());
		ray.add(new Integer(1));
		ray.add(new Integer(2));
		ray.add(new Integer(3));
		ray.add(new Integer(4));
		Diary.log("Ray vide ? ");
		//Diary.logln("" + ray.isEmpty());
		Diary.log("Ray plein : ");
		Diary.logln(ray.toString());
		ArrayList<Integer> ban = ray;
		Diary.log("ban est ray : ");
		Diary.logln(ban.toString());
		ray = new ArrayList<Integer>();
		ray.add(new Integer(2));
		ray.add(new Integer(4));
		Diary.log("ray est pair : ");
		Diary.logln(ray.toString());
		// BUG 2 SPOTTED !
		//ban.removeAll(ray);
		// à la main d'après sources
		// http://lejos.svn.sourceforge.net/viewvc/lejos/trunk/classes/java/util/AbstractCollection.java?revision=2393&view=markup
		for (Integer i : ray) {
			ban.remove(i);
		}
		Diary.log("ban est impair : ");
		Diary.logln(ban.toString());
		
		Diary.terminate();

		// TODO: faudrait aussi vérifier si bug mentionné ici :
		// http://lejos.sourceforge.net/forum/viewtopic.php?t=1257
		// a bien été corrigé. On va essayer de pas se trainer trop de cadavres avec ArrayList
	}

}
