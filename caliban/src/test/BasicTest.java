package test;

import java.util.ArrayList;

import chair.core.CoreNoisyOrModel;
import chair.core.Diary;
import chair.core.Parameters;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;

import lejos.nxt.Button;


public class BasicTest {

	public static void main(String[] args) {
		// Avant toute chose maintenant on note scrupuleusement ce qui nous arrive. 
		Diary.init();
		
		// Un robot a une mauvaise mémoire : on diminue la fenêtre temporelle
		Parameters.setDeltaMax(30);

		// On commence sans trop se prendre la tête : direct le modèle qui va
		// bien avec juste CS/US
		Stimulus CS1 = new Stimulus("CS1", StimulusType.CS);
		ArrayList<Stimulus> stimuli = new ArrayList<Stimulus>();
		stimuli.add(CS1);

		CoreNoisyOrModel model = new CoreNoisyOrModel();
		model.initializeModel(stimuli);

		// Le temps qui passe
		long t = 0;

		System.out.println("Attention on va commencer");
		Button.waitForPress();
		for (int i = 0; i < 30; i++) {
			System.out.println("t = " + t);
			
			// à t=10 la lumière s'allume
			if (i == 1) {
				model.handleEvent(CS1, true);
			}
			model.tick(t);

			System.out.println(Runtime.getRuntime().freeMemory() + "/"
					+ Runtime.getRuntime().totalMemory());
			//Button.waitForPress();
			
			t += Parameters.getStepTime();
		}
		System.out.println(" retour au bercail");
		Button.waitForPress();
	}
}
