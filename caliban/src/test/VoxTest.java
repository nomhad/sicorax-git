package test;

import lejos.nxt.Sound;
import chair.core.Diary;
import robot.calibeta.Voice;

/**
 * Test de la classe qui gère émissions du son par robot
 * 
 * @author nomhad
 * 
 */
public class VoxTest {

	public static void Beep(double a, int b) {
		Sound.playTone((int) a, b);
		Sleep(b);
	}

	public static void Sleep(int b) {
		try {
			Thread.sleep(b);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Voice.Melody.GOOD.play();
		Sleep(2000);
		Voice.Melody.BAD.play();
		Sleep(2000);
		Voice.Melody.ALSO_SPRACH_ZARATHUSTRA.play();
		Sleep(2000);
		Voice.Melody.HELLO.play();
		Sleep(1000);
		Voice.Melody.ZELDA_ITEM_FANFARE.play();
		Sleep(2000);
		Voice.Melody.BEETHOVEN_SHORT.play();
		Sleep(2000);
		Voice.Melody.BEETHOVEN.play();

		Sleep(5000);
		// TODO Auto-generated method stub

		Beep(262 * 2, 500);
		Beep(262 * 2, 200);
		Beep(294 * 2, 500);
		Beep(262 * 2, 1000);
		Beep(349 * 2, 500);
		Beep(330 * 2, 2000);
		Sleep(500);
		Beep(262 * 2, 500);
		Beep(262 * 2, 200);
		Beep(294 * 2, 500);
		Beep(262 * 2, 1000);
		Beep(392 * 2, 500);
		Beep(349 * 2, 1000);
		Sleep(500);
		Beep(262 * 2, 500);
		Beep(262 * 2, 200);
		Beep(440 * 2, 500);
		Beep(349 * 2, 1000);
		Beep(329.7 * 2, 500);
		Beep(329.7 * 2, 1000);
		Beep(293.7 * 2, 500);
		Sleep(500);
		Beep(523.2 * 1.75, 500);
		Beep(523.2 * 1.75, 200);
		Beep(523.2 * 1.65, 500);
		Beep(349.2 * 2, 500);
		Beep(392.0 * 2, 500);
		Beep(349.2 * 2, 1000);

		int duration = 10;

		for (int i = 100; i < 1000; i++) {
			Sound.playTone(i, 50);
			Diary.logln("freq:" + i);
			try {
				Thread.sleep(duration);
			} catch (InterruptedException e) {
			}

		}
		System.exit(0);

		Diary.logln("Start");
		Voice.Melody.ALSO_SPRACH_ZARATHUSTRA.play();

		Diary.logln("wait");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		Diary.logln("hello");

		Voice.Melody.HELLO.play();

		Diary.logln("wait");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		Diary.logln("Fin");
	}
}
