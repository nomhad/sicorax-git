package test;

import chair.Chronos;
import chair.core.Diary;
import robot.Head;
import robot.pavlov.HeadPavlov;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;

public class DebugPavlov {
	private static int iteration = 0;

	public DebugPavlov() {
		// Si on arrive à attraper le robot, il décède immédiatement
		ButtonListener blis = new Tchao();
		Button.ESCAPE.addButtonListener(blis);
		Button.RIGHT.addButtonListener(blis);
		Button.LEFT.addButtonListener(blis);
	}

	public static void printDebug() {
		Diary.logln(iteration + ": Mémoire : + "
				+ Runtime.getRuntime().freeMemory() + " / "
				+ Runtime.getRuntime().totalMemory());
	}

	public static void main(String[] args) {
		// Pour la imple écoute des boutons de la brique
		new DebugPavlov();

		// On initialise logs
		Diary.init();

		// Le temps qui passe
		long temps = Chronos.top();

		printDebug();

		// Initialise le robot principal
		Head teteu = new HeadPavlov();
		while (true) {
			printDebug();
			Diary.logln("\n ----- à t = " + temps + " ----");
			// Les gros calculs sont là
			teteu.getBrain().tick();
			Diary.logln(teteu.getBrain().debug());

			temps = Chronos.top();

			// Attend pour nouvelle itération
			try {
				Thread.sleep(Chronos.getModelStepTime());
			} catch (InterruptedException e) {
			}
		}
	}

	//
	// Listeners, boutons et tactile
	//
	public class Tchao implements ButtonListener {
		public void buttonPressed(Button arg0) {
			Diary.terminate();
			System.exit(0);
		}

		public void buttonReleased(Button arg0) {
		}
	}
}
