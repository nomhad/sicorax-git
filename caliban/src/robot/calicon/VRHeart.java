package robot.calicon;

import robot.calibeta.Voice;
import chair.Brain;
import chair.VisceralReaction;
import chair.core.Diary;

/**
 * Réaction viscérale très simple : se contente de modifier rythme cardiaque
 * 
 * @author nomhad
 * 
 */
public class VRHeart extends VisceralReaction {

	public VRHeart(Brain brain) {
		super(brain);
		// Active les pulsations
		Voice.enableRythm(true);
	}

	@Override
	protected void action(int currentReward) {
		Diary.logln("VRHeart: " + currentReward);
		Voice.setRythm(currentReward);
	}

}
