package robot.calicon;

import chair.Organ;
import robot.calibeta.HeadCalibeta;

/**
 * Une version analogue à HeadCalibetaS, sauf que le robot regarde autour de lui
 * quand on le dépose, et qu'il y a VisceralReaction
 * 
 * @author nomhad
 * 
 */

public class HeadCaliconS extends HeadCalibeta {
	public HeadCaliconS() {
		// Commence par utiliser calibeta en version simple mais sans tendances
		// suicidaires
		super(true, false, false);

		// Le gros du travail : détection de cage
		Organ cage = new OrganCage(_robby);
		// Cet organe nécessite contrôle des roues
		_robby.plug(cage, getMotionFunction());
		// On va lui brancher en plus une réaction viscérale
		new VRHeart(_robby);
	}
}