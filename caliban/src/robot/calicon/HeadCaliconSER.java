package robot.calicon;

import chair.Organ;
import robot.calibeta.HeadCalibeta;

/**
 * Une version analogue à HeadCalibetaS, sauf que le robot regarde autour de lui
 * quand on le dépose, et qu'il y a VisceralReaction.
 * 
 * Version secours avec capteur tactile
 * 
 * @author nomhad
 * 
 */

public class HeadCaliconSER extends HeadCalibeta {
	public HeadCaliconSER() {
		// Commence par utiliser calibeta en version simple mais sans tendances
		// suicidaires
		super(true, false, true);

		// Le gros du travail : détection de cage
		Organ cage = new OrganCage(_robby);
		// Cet organe nécessite contrôle des roues
		_robby.plug(cage, getMotionFunction());
		// On va lui brancher en plus une réaction viscérale
		new VRHeart(_robby);
	}
}