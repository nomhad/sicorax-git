/**
 * Un robot plus évolué. Se repère sur un circuit en mosaïque qu'on aura
 * construit à l'aide des images situées dans caliban/files/tiles.
 * Possibilité de le dresser pour qu'il se rende dans telle pièce plutôt
 * qu'une autre. 
 */
package robot.calibeta;