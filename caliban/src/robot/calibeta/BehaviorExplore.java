package robot.calibeta;

import chair.Behavior;
import chair.Function;
import chair.core.Diary;

/**
 * Cherche à se rendre à une salle inconnue. Ne fait qu'indiquer à Cartographe
 * quelle procédure adopter pour l'itinéraire. Possible de conditionner ce
 * comportement (non spontané donc peut attendre la fin pour se faire).
 * 
 * Détecte le moment où la carte a été entièrement explorée afin de se se
 * déclarer comme périmé et ne plus être sélectionné par la fonction.
 */
public class BehaviorExplore extends Behavior {

	/** C'est à lui qu'on signale l'itinéraire */
	private Cartographer _magellan;

	public BehaviorExplore(Function fct, Cartographer magellan) {
		super(fct, "Explore", true);
		// Enverra front descendant
		setSpontaneous(false);
		_magellan = magellan;
	}

	@Override
	protected void run(boolean toggle) {
		// Si on allume le comportement il signale d'emblée qu'il devra arriver
		// à son terme et indique au cartographe la stratégie à employer pour
		// définir direction.
		if (toggle) {
			setStoppable(false);
			_magellan.setRoute(Cartographer.Route.EXPLORE);
		}
		// Lors de l'extinction, contrôle si restera branches à explorer
		else {
			Diary.logln("Extinction BehaviorExplore");
			if (!_magellan.hasWildJunctions()) {
				Diary.logln("Carte entièrement explorée, désactivation de "
						+ this);
				setRelvevant(false);
			}
		}
	}
}
