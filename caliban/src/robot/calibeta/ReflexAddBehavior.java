package robot.calibeta;

import chair.Behavior;
import chair.Brain;
import chair.Function;
import chair.Impulse;
import chair.Reflex;
import chair.core.Diary;

/**
 * Une nouvelle salle est détectée, ce réflexe est appelé afin de créer le
 * comportement BehaviorReturnRoom associé. Vérifie que c'est une nouvelle salle
 * avant d'agir.
 * 
 * @author nomhad
 * 
 */
public class ReflexAddBehavior extends Reflex {
	/**
	 * Pour ajouter un comportement il faut un lien direct avec cerveau
	 */
	private Brain _robby;
	/**
	 * Comme chaque nouvelle salle incrémente valeur de la sensation il suffit
	 * de garder la plus haute pour savoir si on a déjà traité la salle
	 */
	private byte _lastNewSensing = 0;
	/** Besoin de se rappeler la fonction à passer aux nouveaux comportements */
	private Function _mapFct;
	/** Les nouveaux comportements auront besoin de connaître cartographe */
	private Cartographer _magellan;

	/**
	 * 
	 * @param fct
	 *            fonction associé au réflexe
	 * @param mapFct
	 *            fonction associée aux comportements créés
	 * @param brain
	 *            cerveau auquel ajouter les comportements
	 */
	public ReflexAddBehavior(Function fct, Function mapFct, Brain brain,
			Cartographer magellan) {
		super(fct, "AddBehavior");
		// Utilise réflexe par commodité, pas grand chose à faire sinon
		setTimer(0);
		_mapFct = mapFct;
		_robby = brain;
		_magellan = magellan;
	}

	@Override
	protected void run(Impulse impulse) {
		// Nouvelle salle
		byte value = impulse.getValue();
		if (value > _lastNewSensing) {
			// Crée comportement
			_lastNewSensing = value;
			String roomName = impulse.getSensingName();
			Diary.logln("Création de BehaviorReturnRoom / " + roomName);
			Behavior bev = new BehaviorReturnRoom(_mapFct, roomName, _magellan);
			// On l'ajoute finalement au cerveau
			_robby.addBehavior(bev);
		}
	}
}
