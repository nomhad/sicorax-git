package robot.calibeta;

import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Le robot émet instinctivement un son dès qu'on lui présente un fruit ou un
 * agrume. Prévu pour...pouvoir être prédit. Réflexe immédiat, n'a pas besoin
 * d'être éteint.
 * 
 * Attention, il y a déjà des sons joués par OrganSmell, je n'en fait pas un
 * réflexe, on ne sait pas dans quel ordre tout ça sera joué.
 * 
 * @author nomhad
 * 
 */

public class ReflexVoice extends Reflex {

	/**
	 * Le réflexe se définit comme étant du type RESPONSE (prévisible)
	 * 
	 * @param fct
	 *            fonction associée
	 */
	protected ReflexVoice(Function fct) {
		super(fct, "Voice", ReflexType.RESPONSE);
		setTimer(0);
	}

	@Override
	protected void run(Impulse impulse) {
		if (impulse.getToggle()) {
			Voice.Melody.HELLO.play();
		}

	}

}
