package robot.calibeta;

import chair.core.Diary;
import robot.Substance;

public class CalibetaMain {

	public static void main(String[] args) {
		// On initialise logs
		Diary.init();
		// On indique au substrat quel Brain on veut faire tourner et que nous
		// sommes robotiques. Dès qu'on l'initialise il gardera la main.
		new Substance(new HeadCalibeta().getBrain(), true, false);
	}
}
