package robot.calibeta;

import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Le robot tombe sur un marqueur rouge (la signature d'un carrefour) ou bien
 * sur une salle. Donne la main à Cartographer pour l'explorer nouvel endroit au
 * besoin puis lui demande quelle est la nouvelle direction à prendre. Si aucune
 * alors c'est que l'itinéraire arrive à son terme, un autre réflexe ou un autre
 * comportement prendra le relais.
 */
public class ReflexJunction extends Reflex {

	/**
	 * On signale le carrefour et en échange il nous indiquera quelle direction
	 * prendre
	 */
	private Cartographer _magellan;

	/**
	 * Réflexe qui a quelques responsabilités, d'où les arguments
	 * supplémentaires.
	 * 
	 * @param fct
	 *            Fonction associé au réflexe, classique
	 * @param magellan
	 *            utilisé pour signaler au cartographe qu'on se trouve sur
	 *            nouveau nœud du circuit
	 */
	public ReflexJunction(Function fct, Cartographer magellan) {
		super(fct, "OnJunction");
		setTimer(0);
		_magellan = magellan;
	}

	@Override
	protected void run(Impulse impulse) {
		if (impulse.getToggle()) {
			// Tombe sur un carrefour
			if (impulse.getSensingName().equals("Carrefour"))
				_magellan.onJunction();
			else
			// C'est une salle qui s'est branchée à la volée
			if (impulse.getValue() > 2)
				_magellan.onRoom(impulse.getSensingName());
		}
	}
}
