package robot.calibeta;

import lejos.nxt.ColorLightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.Colors.Color;

/**
 * 
 * Classe destinée à prévenir dépassement de tableau de ColorLightSensor lors
 * d'une mauvaise connexion avec PC. De plus lorsque le robot tourne il n'y a
 * pas de lecture de couleur.
 * 
 * @author nomhad
 * 
 */
public class ColorLightSensorFix extends ColorLightSensor {

	private int col = -1;

	public ColorLightSensorFix(SensorPort port, int type) {
		super(port, type);
		// Qu'il travaille ou pas on regarde rapidement en loucedé où on
		// commence
		col = readValue();
	}

	@Override
	public Color readColor() {
		if (!Motion.isWorking()) {
			col = readValue();
		}
		if (col <= 0 || col >= colorMap.length)
			return Color.NONE;
		return colorMap[col];
	}

}