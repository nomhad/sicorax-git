package robot.calibeta;

import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Quand il atteint un objectif, le robot attend un certain nombre de ms (par
 * défaut : 3 secondes) avant de prendre une nouvelle décision, le temps pour
 * l'utilisateur de lui présenter éventuellement une punition ou une récompense.
 * 
 * @author nomhad
 * 
 */
public class ReflexFreeze extends Reflex {

	public ReflexFreeze(Function fct) {
		this(fct, 3000);
	}

	/**
	 * Avec cette version du constructeur, on peut définir explicitement pendant
	 * combien de temps le robot s'arrête
	 * 
	 * @param fct
	 *            fonction associée au réflexe
	 * @param lapse
	 *            nombre de ms pendant lequel le robot va s'arrêter
	 */
	public ReflexFreeze(Function fct, long lapse) {
		super(fct, "Freeze");
		// le robot se tourne les pouces pendant 3 secondes
		setTimer(lapse);
	}

	@Override
	protected void run(Impulse impulse) {
		// Le robot se repose, seul moment où capteur RFID va mesurer qqchose
		Motion.haveABreak(impulse.getToggle());
	}
}
