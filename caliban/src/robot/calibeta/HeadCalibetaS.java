package robot.calibeta;

/**
 * Version simplifié de calibeta, où le robot ne zigzague pas et où ne se
 * préoccupe pas des branches EAST et WEST. Ne tourne jamais en fait.
 * 
 * TODO: dans Remote utiliser directement HeadCalibeta
 */
public class HeadCalibetaS extends HeadCalibeta {
	public HeadCalibetaS() {
		super(true, true, false);
	}
}
