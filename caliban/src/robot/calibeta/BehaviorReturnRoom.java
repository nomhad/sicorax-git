package robot.calibeta;

import chair.Behavior;
import chair.Function;

/**
 * Cherche à retourner dans une salle déjà rencontrée. Ne fait qu'indiquer à
 * Cartographe quelle procédure adopter pour l'itinéraire. Possible de
 * conditionner ce comportement (non spontané donc peut attendre la fin pour se
 * faire).
 * 
 * Ces comportements sont créés et enregistrés à la volée par ReflexAddBehavior
 */
public class BehaviorReturnRoom extends Behavior {

	/** C'est à lui qu'on signale l'itinéraire */
	private Cartographer _magellan;
	/** Le nom de la salle à retrouver */
	private String _roomName;

	public BehaviorReturnRoom(Function fct, String roomName,
			Cartographer magellan) {
		super(fct, "ReturnRoom" + roomName, true);
		_roomName = roomName;
		// Enverra front descendant
		setSpontaneous(false);
		_magellan = magellan;
	}

	@Override
	protected void run(boolean toggle) {
		// Si on allume le comportement il signale d'emblée qu'il devra arriver
		// à son terme et indique au cartographe la stratégie à employer pour
		// définir direction.
		if (toggle) {
			setStoppable(false);
			_magellan.setRoute(Cartographer.Route.ROOM, _roomName);
		}
	}
}
