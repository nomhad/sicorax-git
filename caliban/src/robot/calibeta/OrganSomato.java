package robot.calibeta;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import chair.Brain;
import chair.Organ;

/** Connaissance interne du robot : est-ce qu'on le soulève ? */
public class OrganSomato extends Organ {
	/** Le capteur tactile */
	private TouchSensor _somato;
	/** Que le capteur tactile soit relâché est notre signal */
	private boolean _isReleased;

	public OrganSomato(Brain brain) {
		super(brain, "OrganSomato");
		_somato = new TouchSensor(SensorPort.S4);
		// Une seule valeur possible
		byte[] values = { 1 };
		registerValues(values);
		// Première lecture avant de nuancer suivant Cartographer
		_isReleased = _somato.isPressed();
	}

	@Override
	protected byte sense() {
		// Pour ne pas encombrer canal on ne mesure rien quand robot tourne
		if (!Motion.isWorking())
			_isReleased = !_somato.isPressed();
		// Par défaut le robot est posé c'est le fait de le soulever (donc de
		// relâché bouton) qui est un signal
		return _isReleased ? (byte) 0 : 1;
	}

}
