package robot.calibeta;

import robot.calibeta.Cartographer.Route;
import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Le robot a atteint un de ses objectif : il revient inexorablement à la niche.
 * Fait calculer itinéraire qui va bien à Cartographer. Ajoute à calibeta du rythme.
 * @author nomhad
 * 
 */
public class ReflexReturnKennel extends Reflex {

	Cartographer _magellan;

	public ReflexReturnKennel(Function fct, Cartographer magellan) {
		super(fct, "ReturnKennel");
		_magellan = magellan;
	}

	@Override
	protected void run(Impulse impulse) {
		if (impulse.getToggle())
			_magellan.setRoute(Route.KENNEL);
	}
}
