package robot.calibeta;

import lejos.nxt.Motor;
import chair.Function;
import chair.Impulse;
import chair.Reflex;
import chair.core.Diary;

/** Le robot décide de passer l'arme à gauche */
public class ReflexSeppuku extends Reflex {

	public ReflexSeppuku(Function fct) {
		super(fct, "Seppuku");
	}

	@Override
	protected void run(Impulse impulse) {
		// Je fais tous dans les règles de l'art
		if (impulse.getToggle()) {
			Diary.logln("Adieu monde cruel !");
			Motor.B.stop();
			Motor.C.stop();
			// Et si autre thread écrit avant instruction qui suit ??

			Diary.terminate();
			System.exit(0);
		}
	}

}
