package robot.calibeta;

import robot.calibeta.Cartographer.JunctionType;
import robot.calibeta.Cartographer.Route;
import chair.Brain;
import chair.Organ;
import chair.core.Diary;

/**
 * Se contente d'interroger Cartographer pour savoir si a atteint son but. Va
 * brièvement envoyer une sensation signalant qu'un but a été atteint (permet
 * d'arrêter robot quelques secondes à chaque objectif), puis les autres fois
 * précisera quel a été ce but. Va donc renvoyer par la suite la même sensation
 * tant qu'un nouveau but n'est pas atteint (permet à ReflexReturnKennel de
 * perdurer tout au long de retour à la niche).
 * 
 * @author nomhad
 * 
 */
public class OrganCartographer extends Organ {
	/** Une destination est-elle atteinte, quel est est la stratégie en cours ? */
	private Cartographer _magellan;
	/** La dernière sensation déterminée */
	private byte _lastAim = 0;
	/** L'objectif vient juste d'être atteint */
	private boolean _newAim = false;

	public OrganCartographer(Brain brain, Cartographer magellan) {
		super(brain, "OrganCartographer");
		_magellan = magellan;
		byte[] values = { 1, 2, 3, 4, 5 };
		String[] names = { "ObjectifAtteint", "RetourneNiche", "ADécouvertSalle",
				"ExplorationTerminée", "AtteintSalle" };
		registerValues(values, names);
	}

	@Override
	protected byte sense() {
		// Un objectif vient d'être atteint, on calcule la sensation qui va
		// perdurer aux prochains tours avant de renvoyer 1 pour cette fois
		if (_magellan.isDestinationReached()) {
			// Le type du dernier carrefour visité et celui de l'itinéraire
			// calcul sont utilisé pour déterminé le type d'objectif atteint
			JunctionType jt = _magellan.getLastJunctionType();
			Route route = _magellan.getRoute();
			// On est sur une niche et ce qu'on voulait
			if (route == Route.KENNEL && jt == JunctionType.KENNEL)
				_lastAim = 2;
			// Découverte d'une salle
			else if (route == Route.EXPLORE && jt == JunctionType.ROOM)
				_lastAim = 3;
			// Fin d'exploration (si n'existe que niche : non géré)
			else if (route == Route.EXPLORE && jt == JunctionType.NODE)
				_lastAim = 5;
			// Voulais une salle, on a une salle. Vérifie pas que c'est la
			// bonne
			else if (route == Route.ROOM && jt == JunctionType.ROOM)
				_lastAim = 5;
			else {
				Diary.logln(this + "Impossible de déterminer l'objectif "
						+ "atteint par Cartographer");
				_lastAim = 0;
			}
			// On ne veut envoyer cette sensation qu'une seule fois
			if (!_newAim) {
				_newAim = true;
				_lastAim = 1;
			}
		} else if (_newAim)
			_newAim = false;
		return _lastAim;
	}
}
