package robot.calibeta;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import chair.Brain;
import chair.Organ;
import chair.core.Diary;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;

/**
 * Version de secours de OrganSmell avec juste un capteur tactile pour
 * récompense
 * 
 * @author nomhad
 * 
 */
public class OrganSmellER extends Organ {

	/** Le capteur tactile dirigé vers l'avant sert d'odora */
	private TouchSensor _smell;
	/** Que le capteur tactile soit enfoncé est notre signal */
	private boolean _isPressed;
	/** On ne joue une mélodie que la première fois que la pression est détectée */
	private boolean _hasBeenPressed = false;

	public OrganSmellER(Brain brain) {
		super(brain, "OrganSmellER");
		_smell = new TouchSensor(SensorPort.S4);
		// 1: fraise, 2: citron
		byte[] values = { 1 };
		String[] names = { "MiamFraise" };
		registerValues(values, names);
		setStimulus(new Stimulus("Fraise", StimulusType.US, 1), (byte) 1);
		setSpontaneous(true);
		// Une lecture lors d'initialisation
		_isPressed = _smell.isPressed();
	}

	/**
	 * Si une lecture est effectuée, ne renverra plus 0 tant que Motion est en
	 * break.
	 */
	@Override
	protected byte sense() {
		// Pour ne pas encombrer canal on ne mesure rien quand robot tourne
		if (!Motion.isWorking()) {
			_isPressed = _smell.isPressed();
			if (_isPressed) {
				// On joue une mélodie seulement quand le bouton s'enfonce
				if (!_hasBeenPressed) {
					_hasBeenPressed = true;
					Diary.logln(this + " : miam de la fraise !");
					Voice.Melody.ZELDA_ITEM_FANFARE.play();
				}
			} else
				_hasBeenPressed = false;

		}
		return _isPressed ? (byte) 1 : 0;
	}
}
