package robot.skinner;

import lejos.nxt.ColorLightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.Colors.Color;
import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Dès qu'on le punit le robot, estomaqué, ne produit plus aucune lumière
 * 
 * @author nomhad
 * 
 */
public class ReflexLumiereStop extends Reflex {

	private ColorLightSensor _light;

	public ReflexLumiereStop(Function fct) {
		// On laisse le soin à maman d'enregistrer Function
		super(fct, "Lumiere");
		// Cet organe brille de mille feux
		_light = new ColorLightSensor(SensorPort.S3,
				ColorLightSensor.TYPE_COLORNONE);
	}

	@Override
	protected void run(Impulse impulse) {
		if (impulse.getToggle()) {
			_light.setFloodlight(Color.NONE);
		}
	}
}
