package robot.skinner;

import chair.Brain;
import chair.Organ;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

/**
 * Capteur tactile branché sur port S4 qui induit une punition lorsqu'il est
 * enfoncé
 * 
 * @author nomhad
 * 
 */
public class OrganTartare extends Organ {

	private TouchSensor _bumperTartare;

	public OrganTartare(Brain brain) {
		super(brain, "ToucherTartare");
		// Afin d'être sûr de capturer collision on utilise listener
		_bumperTartare = new TouchSensor(SensorPort.S4);
		// Une seule valeur possible
		byte[] values = {1};
		registerValues(values);
		// Cet organe permet de savoir si on tape un mur
		setStimulus(new Stimulus("ToucherTartare", StimulusType.US, -1));
		setSpontaneous(true);
	}

	@Override
	protected byte sense() {
		return _bumperTartare.isPressed() ? (byte) 1 : 0;
	}

}
