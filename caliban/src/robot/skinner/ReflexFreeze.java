package robot.skinner;

import lejos.nxt.Motor;
import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Dès qu'on le stimule, le robot reste pétrifié
 * 
 * @author nomhad
 * 
 */
public class ReflexFreeze extends Reflex {

	public ReflexFreeze(Function fct) {
		super(fct, "Freeze");
	}

	@Override
	protected void run(Impulse impulse) {
		// S'arrêter ou ne rien faire, il faut choisir
		if (impulse.getToggle()) {
			Motor.B.stop();
			Motor.C.stop();
		}
	}
}
