package robot.skinner;

import chair.Brain;
import chair.Organ;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

/**
 * Capteur tactile branché sur port S1 qui induit une récompense lorsqu'il est
 * enfoncé
 * 
 * @author nomhad
 * 
 */
public class OrganEros extends Organ {

	private TouchSensor _bumperEros;

	public OrganEros(Brain brain) {
		super(brain, "ToucherEros");
		// Afin d'être sûr de capturer collision on utilise listener
		_bumperEros = new TouchSensor(SensorPort.S1);
		// Une seule valeur possible
		byte[] values = {1};
		registerValues(values);
		// Cet organe permet de savoir si on tape un mur
		setStimulus(new Stimulus("ToucherEros", StimulusType.US, 1));
		setSpontaneous(true);
	}

	@Override
	protected byte sense() {
		return _bumperEros.isPressed() ? (byte) 1 : 0;
	}

}
