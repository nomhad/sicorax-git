package robot.skinner;

import chair.core.Diary;
import robot.Substance;

public class SkinnerMain {

	public static void main(String[] args) {
		// On initialise logs
		Diary.init();
		// On indique au substrat quel Brain on veut faire tourner et que nous
		// sommes robotiques. Dès qu'on l'initialise il gardera la main.
		new Substance(new HeadSkinner().getBrain(), true, false);
	}
}
