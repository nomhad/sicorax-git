package robot.skinner;

import robot.Head;
import chair.Behavior;
import chair.Brain;
import chair.Chronos;
import chair.Function;
import chair.Organ;
import chair.Reflex;
import chair.core.CoreNoisyOrModel;

/**
 * Permet de factoriser le code entre les différentes version de Pavlov.
 * Retourne un Brain que le programme n'a plus qu'à faire faire tick() après
 * d'éventuelles modifications.
 * 
 * @author nomhad
 * 
 */
public class HeadSkinner extends Head {
	/** Initialisation du modèle et du Brain */
	public HeadSkinner() {
		// On règle les paramètres de l'expérience
		Chronos.setModelDeltaMax(5000);
		Chronos.setModelStepTime(350);

		// Initialisation du modèle
		_model = new CoreNoisyOrModel();
		// Paramètres modèle
		_model.setAlpha(0.4);
		_model.setForecastThreshold(0.4);

		// Robby devient un être à part entière
		_robby = new Brain(_model);
		// Initialisation des organes
		Organ toucherTartare = new OrganTartare(_robby);
		Organ toucherEros = new OrganEros(_robby);
		// Initialisation des fonctions du robot
		Function mouvance = new Function(_robby, "Se mouvoir");
		Function enluminures = new Function(_robby, "Capteur lumiere");
		// Initilisation du comportement du robot
		Behavior avance = new BehaviorAvance(mouvance);
		Behavior rouge = new BehaviorRed(enluminures);
		Behavior bleu = new BehaviorBlue(enluminures);
		// Initialisation des réflexes du robot
		Reflex lumiereStop = new ReflexLumiereStop(enluminures);
		Reflex lumiereGreen = new ReflexLumiereGreen(enluminures);
		Reflex freeze = new ReflexFreeze(mouvance);
		// On organise tout ça sur le robot maintenant : ajout comportements,
		// organes et liaison de ces derniers avec leurs réflexes
		_robby.addBehavior(avance);
		_robby.addBehavior(rouge);
		_robby.addBehavior(bleu);
		_robby.plug(toucherEros, lumiereGreen);
		_robby.plug(toucherEros, freeze);
		_robby.plug(toucherTartare, lumiereStop);
		_robby.plug(toucherTartare, freeze);
	}
}
