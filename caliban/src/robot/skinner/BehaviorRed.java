package robot.skinner;

import lejos.nxt.ColorLightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.Colors.Color;
import chair.Behavior;
import chair.Function;

/**
 * Le robot peut choisir d'allumer sa loupiote rouge
 * 
 * @author nomhad
 * 
 */
public class BehaviorRed extends Behavior {

	private ColorLightSensor _light;

	public BehaviorRed(Function fct) {
		super(fct, "ToutRouge", true);
		// Cet organe brille de mille feux
		_light = new ColorLightSensor(SensorPort.S3,
				ColorLightSensor.TYPE_COLORNONE);
	}

	@Override
	protected void run(boolean toggle) {
		if (toggle) {
			_light.setFloodlight(Color.RED);
		}
	}
}
