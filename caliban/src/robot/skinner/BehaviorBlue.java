package robot.skinner;

import lejos.nxt.ColorLightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.Colors.Color;
import chair.Behavior;
import chair.Function;

/**
 * Le robot peut choisir d'allumer sa loupiote bleue
 * 
 * @author nomhad
 * 
 */
public class BehaviorBlue extends Behavior {

	private ColorLightSensor _light;

	public BehaviorBlue(Function fct) {
		super(fct, "ToutBleu", true);
		// Cet organe brille de mille feux
		_light = new ColorLightSensor(SensorPort.S3,
				ColorLightSensor.TYPE_COLORNONE);
	}

	@Override
	protected void run(boolean toggle) {
		if (toggle) {
			_light.setFloodlight(Color.BLUE);
		}
	}
}
