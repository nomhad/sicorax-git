package robot.pavlov;

import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import chair.Brain;
import chair.Organ;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;

/**
 * Capteur ultrasonic branché sur port S2 qui permet de savoir si passe sous un
 * obstacle
 * 
 * @author nomhad
 * 
 */
public class OrganVue extends Organ {

	private UltrasonicSensor _sonic;
	/** obstacle significatif seulement s'il est à moins de 30 centimètres */
	private final int DISTANCE_OBSTACLE = 30;

	public OrganVue(Brain brain) {
		super(brain, "Vue");
		// Une seule valeur possible
		byte[] values = {1};
		registerValues(values);
		// Cet organe permet de savoir si on passe sous un pont
		setStimulus(new Stimulus("PasseSousPont", StimulusType.CS));
		setSpontaneous(true);
		// Initialisation du senseur
		_sonic = new UltrasonicSensor(SensorPort.S2);
	}

	@Override
	protected byte sense() {
		if (_sonic.getDistance() <= DISTANCE_OBSTACLE) {
			return 1;
		}
		return 0;
	}
}
