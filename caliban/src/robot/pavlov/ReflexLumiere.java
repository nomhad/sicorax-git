package robot.pavlov;

import lejos.nxt.ColorLightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.Colors.Color;
import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Capteur de lumière branché sur port S3 qui représente la réponse du robot à
 * l'US
 * 
 * @author nomhad
 * 
 */
public class ReflexLumiere extends Reflex {

	private ColorLightSensor _light;

	public ReflexLumiere(Function fct) {
		// On laisse le soin à maman d'enregistrer Function. RESPONSE : le
		// "chien" peut "baver" avant d'arriver effectivement à nourriture
		super(fct, "Lumiere", ReflexType.RESPONSE);
		// Cet organe brille de mille feux
		_light = new ColorLightSensor(SensorPort.S3,
				ColorLightSensor.TYPE_COLORNONE);
	}

	@Override
	protected void run(Impulse impulse) {
		if (impulse.getToggle()) {
			_light.setFloodlight(Color.GREEN);
		} else
			_light.setFloodlight(Color.NONE);
	}
}
