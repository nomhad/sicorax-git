package robot.pavlov;

import lejos.nxt.Motor;
import chair.Behavior;
import chair.Function;
import chair.core.Diary;

/**
 * Le robot n'a pas un vaste choix de comportement : il avance tant qu'il peut,
 * bille en tête.
 * 
 * @author nomhad
 * 
 */
public class BehaviorAvance extends Behavior {

	public BehaviorAvance(Function fct) {
		super(fct,"Avance", false);
	}

	@Override
	protected void run(boolean toggle) {
		Diary.logln(_name + toggle);
		if (toggle) {
			Motor.B.forward();
			Motor.C.forward();
		}
	}
}
