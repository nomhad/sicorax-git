package robot.pavlov;

import lejos.nxt.Motor;
import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Lorsque le robot rencontre un obstacle, après un arrêt il ne pourra
 * s'empêcher de reculer
 * 
 * @author nomhad
 * 
 */
public class ReflexReculer extends Reflex {

	public ReflexReculer(Function fct) {
		super(fct, "Reculer");
		// le robot reculera pendant 3 secondes
		setTimer(3000);
	}

	@Override
	protected void run(Impulse impulse) {
		// Il recule...
		if (impulse.getToggle()) {
			Motor.B.backward();
			Motor.C.backward();
		}
		// Si doit arrêter son action, ce sera en arrêtant de reculer. Ce robot
		// n'a pas froid aux yeux.
		else {
			Motor.B.stop();
			Motor.C.stop();
		}
	}
}
