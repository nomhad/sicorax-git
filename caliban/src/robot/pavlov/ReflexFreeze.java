package robot.pavlov;

import lejos.nxt.Motor;
import chair.Function;
import chair.Impulse;
import chair.Reflex;

/**
 * Quand il rencontre un obstacle, le robot reste pétrifié pendant 3 secondes.
 * Dans la signification qu'on donne à cet obstacle, il lui faut bien ça pour
 * manger.
 * 
 * @author nomhad
 * 
 */
public class ReflexFreeze extends Reflex {

	public ReflexFreeze(Function fct) {
		super(fct, "Freeze");
		// le robot s'arrête pendant 3 secondes
		setTimer(3000);
	}

	@Override
	protected void run(Impulse impulse) {
		// S'arrêter ou ne rien faire, il faut choisir
		if (impulse.getToggle()) {
			Motor.B.stop();
			Motor.C.stop();
		}
	}
}
