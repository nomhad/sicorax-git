package robot.pavlov;

import chair.Brain;
import chair.Organ;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

/**
 * Capteur tactile branché sur port S1 qui permet de savoir si se cogne contre
 * un mur
 * 
 * @author nomhad
 * 
 */
public class OrganToucher extends Organ {

	private TouchSensor _bumper;

	public OrganToucher(Brain brain) {
		super(brain, "Toucher");
		// Afin d'être sûr de capturer collision on utilise listener
		_bumper = new TouchSensor(SensorPort.S1);
		// Une seule valeur possible
		byte[] values = {1};
		registerValues(values);
		// Cet organe permet de savoir si on tape un mur
		setStimulus(new Stimulus("TapeMur", StimulusType.US));
		setSpontaneous(true);
	}

	@Override
	protected byte sense() {
		return _bumper.isPressed() ? (byte)1 : 0;
	}

}
