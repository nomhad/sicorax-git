package robot.pavlov;

import robot.Head;
import chair.Behavior;
import chair.Brain;
import chair.Chronos;
import chair.Function;
import chair.Organ;
import chair.Reflex;
import chair.core.CoreNoisyOrModel;

/**
 * Permet de factoriser le code entre les différentes version de Pavlov.
 * Retourne un Brain que le programme n'a plus qu'à faire faire tick() après
 * d'éventuelles modifications.
 * 
 * @author nomhad
 * 
 */
public class HeadPavlov extends Head {
	/** Initialisation du modèle et du Brain */
	public HeadPavlov() {
		// On règle les paramètres de l'expérience
		Chronos.setModelDeltaMax(5000);
		Chronos.setModelStepTime(350);

		// Initialisation du modèle
		_model = new CoreNoisyOrModel();
		// Paramètres modèle
		_model.setAlpha(0.4);
		_model.setForecastThreshold(0.4);
		// FIXME: sur ce modèle faire décroître nouveauté avec ce coeff affaibli
		// très rapidement stimuli jusqu'à les rendre rapidement non
		// significatifs
		// model.setDecreasingStrengh(true); // par défaut dans modèle
		// model.setDecreasingStrenghSpeed(0.5);// k3

		// Robby devient un être à part entière
		_robby = new Brain(_model);
		// Initialisation des organes
		// Organ vue = new OrganVue(_robby);
		new OrganVue(_robby);
		Organ toucher = new OrganToucher(_robby);
		// Initialisation des fonctions du robot
		Function mouvance = new Function(_robby, "Se mouvoir");
		Function enluminures = new Function(_robby, "Capteur lumiere");

		// Initilisation du comportement du robot
		Behavior avance = new BehaviorAvance(mouvance);
		// Initialisation des réflexes du robot
		Reflex lumiere = new ReflexLumiere(enluminures);
		Reflex freeze = new ReflexFreeze(mouvance);
		Reflex reculer = new ReflexReculer(mouvance);
		// On organise tout ça sur le robot maintenant : ajout comportements,
		// organes et liaison de ces derniers avec leurs réflexes
		_robby.addBehavior(avance);
		_robby.plug(toucher, lumiere);
		// Attention, ordre important : celui de l'éxécution pour une fonction
		_robby.plug(toucher, freeze);
		_robby.plug(toucher, reculer);
	}
}
