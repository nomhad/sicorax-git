/**
 * Un robot qui avance jusqu'à rencontrer obstacle.
 * Il s'arrête alors pour manger (lumière verte s'allume).
 * Recule, recommence. Si prévoit que va rencontrer gamelle, allume lumière verte.
 */
package robot.pavlov;