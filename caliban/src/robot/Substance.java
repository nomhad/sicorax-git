package robot;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import chair.Brain;
import chair.Chronos;
import chair.core.Diary;

/**
 * Attention, dès l'instanciation cette classe ne rend plus la main. C'est elle
 * qui via une boucle infinie va faire tourner le cerveau. Dépend de deux
 * classes de lejos.nxt pour le cas où lancé depuis brique (écoute des boutons).
 * 
 * @author nomhad
 * 
 */
public class Substance {
	/**
	 * Lors de la construction on indique si le programme tourne depuis la
	 * brique NXT (true) ou depuis le PC (false). Il n'y a en effet pas la même
	 * interaction possible (boutons de la brique ou clavier).
	 */
	private boolean _isRobot;
	/**
	 * Par défaut va afficher le moins d'information de debug possible
	 */
	private boolean _debug = false;
	/**
	 * Une fois que Head a effectué le branchement on a besoin de connaître que
	 * l'emplacement du cerveau ici
	 */
	private Brain _robby;

	/**
	 * Dès la construction les listeners éventuels sont créés (debug on/off,
	 * exit) puis une boucle perpétuelle est engagée pour faire tourner le
	 * modèle.
	 * 
	 * @param robby
	 *            Brain à faire tourner
	 * @param isRobot
	 *            true si lancé depuis NXT, false sinon
	 * @param debug
	 *            true si doit afficher des informations de debug étendu (sur le
	 *            modèle)
	 */
	public Substance(Brain robby, boolean isRobot, boolean debug) {
		_debug = debug;
		_robby = robby;
		_isRobot = isRobot;
		if (_isRobot) {
			// Si on arrive à attraper le robot, il décède immédiatement
			ButtonListener blis = new Tchao();
			Button.ESCAPE.addButtonListener(blis);
			// On appuie sur la flêche gauche pour afficher debug, flêche droite
			// pour désactiver
			ButtonListener dDebugblis = new DisableDebug();
			Button.RIGHT.addButtonListener(dDebugblis);
			ButtonListener eDebugblis = new EnableDebug();
			Button.LEFT.addButtonListener(eDebugblis);
		}

		// Le temps qui passe
		long temps = Chronos.top();

		// 3...2...1...Ignition !
		while (true) {
			if (_debug)
				Diary.logln("\n ----- à t = " + temps + " ----");

			// Les gros calculs sont là
			_robby.tick();

			if (_debug)
				Diary.logln(_robby.debug());

			// N'utilise pas Parameters.stepTime
			temps = Chronos.top();

			// Attend pour nouvelle itération
			try {
				Thread.sleep(Chronos.getModelStepTime());
			} catch (InterruptedException e) {
			}
		}
	}

	//
	// Listeners, boutons et tactile
	//
	public class Tchao implements ButtonListener {
		public void buttonPressed(Button arg0) {
			Diary.terminate();
			System.exit(0);
		}

		public void buttonReleased(Button arg0) {
		}
	}

	public class EnableDebug implements ButtonListener {
		public void buttonPressed(Button arg0) {
			_debug = true;
		}

		public void buttonReleased(Button arg0) {
		}
	}

	public class DisableDebug implements ButtonListener {
		public void buttonPressed(Button arg0) {
			_debug = false;
		}

		public void buttonReleased(Button arg0) {
		}
	}

}
