package robot.context;

import chair.core.Diary;
import robot.Substance;

public class ContextMain {

	public static void main(String[] args) {
		// On initialise logs
		Diary.init();
		// On indique au substrat quel Brain on veut faire tourner et que nous
		// sommes robotiques. Dès qu'on l'initialise il gardera la main.
		new Substance(new HeadContext().getBrain(), true, false);
	}
}
