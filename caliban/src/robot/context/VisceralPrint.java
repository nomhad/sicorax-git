package robot.context;

import chair.Brain;
import chair.VisceralReaction;

/**
 * Le robot se contente bêtement d'afficher sur son écran son degré de
 * satisfaction
 * 
 * @author nomhad
 * 
 */
public class VisceralPrint extends VisceralReaction {

	public VisceralPrint(Brain brain) {
		super(brain);
	}

	@Override
	protected void action(int currentReward) {
		System.out.println("** Je suis " + currentReward + " **");
	}

}
