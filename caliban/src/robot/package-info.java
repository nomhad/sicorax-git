/**
 * Ce paquet regroupe l'ensemble des expériences mises en place.
 * Dans chacune d'elles la classe Main correspondante est destinée 
 * à être charger sur la brique NXT. On économise ainsi au maximum
 * la place, inutile de compiler ce dont on aura pas besoin.
 * 
 * C'est Remote qu'il faudra executer depuis le PC. Contient deux
 * classes supplémentaire : Head, dont les classes filles construiront
 * le robot, et Substance, qui fait tourner le Brain qu'on lui donne
 * en paramètre.
 */
package robot;