package robot;

import chair.Brain;
import chair.core.CoreNoisyOrModel;

/**
 * La tête d'un robot. C'est le constructeur de la classe fille qui va créer et
 * brancher toutes les parties du robot. Elle devra renseigner les champs
 * CoreModel _model et Brain _robby. Il y a depuis l'extérieur (en particulier
 * depuis Remote) ainsi à disposition un moyen générique d'accéder au Brain
 * correspondant via getBrain(). Il ne estera plus qu'à l'envoyer à Substance
 * pour qu'il prenne vie.
 * 
 * @author nomhad
 * 
 */
public abstract class Head {
	/** Le modèle de conditionnement à utiliser */
	protected CoreNoisyOrModel _model;
	/** Pourra triturer comme on veut le robot une fois initialisé */
	protected Brain _robby;

	/**
	 * Retourne le cerveau du robot, fin prêt à être utilisé
	 * 
	 * @return cerveau qui vient d'être construit
	 */
	public Brain getBrain() {
		return _robby;
	}
}
