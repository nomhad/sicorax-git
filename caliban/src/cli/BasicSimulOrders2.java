package cli;

import java.util.ArrayList;

import chair.core.CoreNoisyOrModel;
import chair.core.Diary;
import chair.core.Parameters;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;

public class BasicSimulOrders2 {

	/**
	 * On va essayer de se produire une petite simulation avec amour directement
	 * à la main. Là plus particulièrement on voudrait donner des ordres au
	 * robots.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Ouvrira lien vers console au besoin
		Diary.init();

		// Un robot a une mauvaise mémoire : on diminue la fenêtre temporelle
		Parameters.setDeltaMax(50);

		// On commence sans trop se prendre la tête : direct le modèle qui va
		// bien avec juste CS/US
		Stimulus OR1 = new Stimulus("ordre1", StimulusType.CS);
		Stimulus B1 = new Stimulus("b1", StimulusType.CS);
		Stimulus B2 = new Stimulus("B2", StimulusType.CS);
		Stimulus OR2 = new Stimulus("ordre2", StimulusType.CS);
		Stimulus BE2 = new Stimulus("b2", StimulusType.CS);
		Stimulus US = new Stimulus("US", StimulusType.US);
		Stimulus US2 = new Stimulus("US2", StimulusType.US);
		ArrayList<Stimulus> stimuli = new ArrayList<Stimulus>();
		stimuli.add(OR1);
		stimuli.add(B1);
		stimuli.add(B2);
		stimuli.add(OR2);
		stimuli.add(BE2);
		stimuli.add(US);
		stimuli.add(US2);

		CoreNoisyOrModel model = new CoreNoisyOrModel();
		// Paramètres modèle
		model.setAlpha(0.4);
		model.setDecreasingStrengh(false);
		model.initializeModel(stimuli);
		// model.plugStimulus(CS1);
		// model.plugStimulus(US);

		// Le temps qui passe
		long t = 0;

		// Première itération à t=0 puis 99 autres vont suivre
		for (int i = 0; i < 100; i++) {
			// à t=10 la lumière s'allume
			if (i == 1) {
				model.handleEvent(OR1, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 2) {
				model.handleEvent(B1, true);
			}
			if (i == 3) {
				model.handleEvent(B1, false);
			}
			// entre t=200 et t=250 une corne de brume
			if (i == 4) {
				model.handleEvent(US, true);
			}

			// à t=10 la lumière s'allume
			if (i == 11) {
				model.handleEvent(OR2, true);
			}
			if (i == 12) {
				model.handleEvent(B1, true);
			}
			if (i == 13) {
			//	model.handleEvent(B1, false);
			}
			// à t=100 la lumière s'éteind
			if (i == 14) {
			//	model.handleEvent(US2, true);
			}

			// à t=10 la lumière s'allume
			if (i == 21) {
				model.handleEvent(OR2, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 22) {
				model.handleEvent(B1, true);
			}
			if (i == 23) {
			//	model.handleEvent(B1, false);
			}
			// entre t=200 et t=250 une corne de brume
			if (i == 24) {
				//model.handleEvent(US2, true);
			}

			// à t=10 la lumière s'allume
			if (i == 31) {
				model.handleEvent(OR1, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 32) {
				model.handleEvent(B1, true);
			}
			if (i == 33) {
				model.handleEvent(B1, false);
			}
			// à t=100 la lumière s'éteind
			if (i == 34) {
				model.handleEvent(US, true);
			}

			// à t=10 la lumière s'allume
			if (i == 41) {
				model.handleEvent(OR1, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 42) {
				model.handleEvent(B2, true);
			}
			if (i == 43) {
				//model.handleEvent(B2, false);
			}
			// à t=100 la lumière s'éteind
			if (i == 44) {
				//model.handleEvent(US2, true);
			}

			// à t=10 la lumière s'allume
			if (i == 51) {
				model.handleEvent(OR2, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 52) {
				model.handleEvent(B1, true);
			}
			if (i == 53) {
			//	model.handleEvent(B1, false);
			}
			// entre t=200 et t=250 une corne de brume
			if (i == 54) {
			//	model.handleEvent(US2, true);
			}

			// à t=10 la lumière s'allume
			if (i == 61) {
				model.handleEvent(OR2, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 62) {
				model.handleEvent(B2, true);
			}
			if (i == 63) {
				model.handleEvent(B2, false);
			}
			// entre t=200 et t=250 une corne de brume
			if (i == 64) {
				model.handleEvent(US, true);
			}

			// à t=10 la lumière s'allume
			if (i == 71) {
				model.handleEvent(OR2, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 72) {
				model.handleEvent(B2, true);
			}
			if (i == 73) {
				model.handleEvent(B2, false);
			}
			// entre t=200 et t=250 une corne de brume
			if (i == 74) {
				model.handleEvent(US, true);
			}

			// à t=10 la lumière s'allume
			if (i == 81) {
				model.handleEvent(OR1, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 82) {
				model.handleEvent(B2, true);
			}
			if (i == 83) {
			//	model.handleEvent(B2, false);
			}
			// à t=100 la lumière s'éteind
			if (i == 84) {
			//	model.handleEvent(US2, true);
			}

			// à t=10 la lumière s'allume
			if (i == 91) {
				model.handleEvent(OR1, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 92) {
				model.handleEvent(B1, true);
			}
			if (i == 93) {
				model.handleEvent(B1, false);
			}
			// à t=100 la lumière s'éteind
			if (i == 94) {
				break;
			}

			model.tick(t);
			Diary.logln("\nà t = " + t);

			// Premier modèle

			Diary.logln("=== model ===");
			Diary.logln(model.debug());
			Diary.logln("Prédiction US " + model.checkForecast(US));
			Diary.logln("Prédiction US2 " + model.checkForecast(US2));

			t += Parameters.getStepTime();
		}
		// Referme journal
		Diary.terminate();
	}
}
