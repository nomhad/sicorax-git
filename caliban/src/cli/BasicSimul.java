package cli;

import java.util.ArrayList;

import chair.core.CoreNoisyOrModel;
import chair.core.Diary;
import chair.core.Parameters;
import chair.core.Stimulus;
import chair.core.Stimulus.StimulusType;


public class BasicSimul {

	/**
	 * On va essayer de se produire une petite simulation avec amour directement
	 * à la main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Ouvrira lien vers console au besoin
		Diary.init();
		
		// Un robot a une mauvaise mémoire : on diminue la fenêtre temporelle
		Parameters.setDeltaMax(300);
		
		// On commence sans trop se prendre la tête : direct le modèle qui va
		// bien avec juste CS/US
		Stimulus CS1 = new Stimulus("CS1", StimulusType.CS);
		Stimulus US = new Stimulus("US", StimulusType.US);
		ArrayList<Stimulus> stimuli = new ArrayList<Stimulus>();
		stimuli.add(CS1);
		stimuli.add(US);
		
		CoreNoisyOrModel model = new CoreNoisyOrModel();
		//model.initializeModel(stimuli);
		model.plugStimulus(CS1);
		model.plugStimulus(US);
		
		// Le temps qui passe
		long t = 0;

		// Première itération à t=0 puis 99 autres vont suivre
		for(int i = 0; i < 100; i++)
		{
			// à t=10 la lumière s'allume
			if (i == 1) {
				model.handleEvent(CS1, true);
			}
			// à t=100 la lumière s'éteind
			if (i == 10) {
				model.handleEvent(CS1, false);
			}
			// entre t=200 et t=250 une corne de brume
			if (i == 20) {
				model.handleEvent(US, true);
			}
			if (i == 25) {
				model.handleEvent(US, false);
			}
			
			// Attention, deuxième passage
			
			// à t=600 la lumière s'allume
			if (i == 60) {
				model.handleEvent(CS1, true);
			}
			// à t=650 la lumière s'éteind
			if (i == 65) {
				model.handleEvent(CS1, false);
			}
			// à t=680 corne de brume qui n'en finit plus
			if (i == 68) {
				model.handleEvent(US, true);
			}
			
			model.tick(t);
			Diary.logln("\nà t = " + t);
			
			
			// Premier modèle
			
			Diary.logln("=== model ===");
			Diary.logln(model.debug());
			Diary.logln("Prédiction US " + model.checkForecast(US));
						
			t += Parameters.getStepTime();
		}
		// Referme journal
		Diary.terminate();
	}
}
