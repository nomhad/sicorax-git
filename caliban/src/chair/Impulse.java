package chair;

/**
 * Cette classe est le pendant de Stimulus, destiné aux réflexe. Permet
 * d'étendre l'information qu'on leur fournit lorsqu'on leur donne un ordre. Un
 * "influx nerveux" est à la fois porteur de l'ordre activation/extinction mais
 * aussi du sens : quelle sensation l'a déclenché.
 * 
 * Quand le réflexe réagit différemment suivant la sensation, attention à ce que
 * sa connaissance de l'ogane soit bonne. On peut facilement le brancher à
 * n'importe quel organe et dès lors le perdre.
 * 
 * @author nomhad
 * 
 */
final public class Impulse implements Cloneable {
	/** Allumer ou éteindre réflexe */
	private boolean _toggle;
	/** Sensation brute de l'organe à l'origine de ce tordre */
	private byte _value;
	/** Nom parfois plus explicite de cette sensation */
	private String _sensingName;

	/**
	 * Le minimum vital, utilisé par Organ dans cas de LOCK.
	 * 
	 * @param toggle
	 *            éteindre ou allumer
	 */
	Impulse(boolean toggle) {
		_toggle = toggle;
	}

	/**
	 * Pour un réflexe au poil soyeux, utiliser ce constructeur.
	 * 
	 * @param toggle
	 *            éteindre ou allumer réflexe
	 * @param value
	 *            valeur brute de la sensation à l'origine de l'ordre
	 * @param sensingName
	 *            nom parfois plus précis que donne l'organe
	 */
	Impulse(boolean toggle, byte value, String sensingName) {
		this(toggle);
		_sensingName = sensingName;
		_value = value;
	}

	@Override
	public String toString() {
		return "Impulse [_sensingName=" + _sensingName + ", _toggle=" + _toggle
				+ ", _value=" + _value + "]";
	}

	/**
	 * Cette impulsion est-elle destinée à éteindre ou à allumer réflexe ?
	 * 
	 * @return true si réflexe va être allumé, false sinon
	 */
	public boolean getToggle() {
		return _toggle;
	}

	/**
	 * Utilisé par Function.ReflexCommand.extinct() afin d'envoyer explicitement
	 * un ordre d'extinction au réflexe courant.
	 * 
	 * @param toggle
	 */
	void setToggle(boolean toggle) {
		_toggle = toggle;
	}

	/**
	 * Description souvent plus complète de la sensation activant ce réflexe.
	 * 
	 * @return paramètre sensingName retourné à l'origine par l'organe
	 */
	public String getSensingName() {
		return _sensingName;
	}

	/**
	 * Valeur brute de la sensation activant ce réflexe.
	 * 
	 * @return paramètre value retourné à l'origine par l'organe
	 */
	public byte getValue() {
		return _value;
	}

	@Override
	public Object clone() {
		Impulse imp = null;
		try {
			imp = (Impulse) super.clone();
		} catch (CloneNotSupportedException e) {
		}
		return imp;
	}
}
