package chair;

/**
 * VisceralReaction permet à l'utilisateur de définir le comportement du robot
 * en fonction de son niveau de stress. C'est Hippocampus qui appelera fire()
 * lors de chaque modification.
 * 
 * Le niveau de stress est codé sur un entier, plus il est négatif plus il est
 * sous pression, plus il lorgne vers l'immensément grand plus il est aux anges.
 * 
 * NB: l'appel viendra de Brain.handleEvent(), mais comme théoriquement on
 * associe un "reward" seulement à un US produit par un organ, c'est en fin de
 * compte le thread Thalamus qui va tout prendre sous son aile. Oui, il faut
 * commencer à s'accrocher un peu pour suivre le déroulement du programme.
 * 
 * TODO: pourrait considérer un organe Visceral (tient, un air de déjà-vu ?)
 * avec des réflexes viscéreaux ajoutés par l'utilisateur encore plus
 * prioritaires que réflexes normaux, un nouveau ReflexType qui aurait primerait
 * sur tout.
 * 
 * @author nomhad
 * 
 */
public abstract class VisceralReaction {
	/** The Brain, film dont je n'ai que de vagues souvenirs */
	private Brain _brain;

	public VisceralReaction(Brain brain) {
		_brain = brain;
		// S'enregistre à qui de droit
		_brain.getHippocampus().plugVisceralReaction(this);
	}

	/**
	 * La réaction de l'organisme à un certain niveau de stress codé sous forme
	 * d'entier.
	 * 
	 * ATTENTION: pas de Function ici donc il faut bien prendre garde à ce que
	 * les composants manipulés n'entrent pas en collision avec des
	 * réflexes/comportements. Sont exclusifs à VisceralReaction.
	 * 
	 * @param currentReward
	 *            niveau de "stress" actuel du robot
	 */
	protected abstract void action(int currentReward);
}
