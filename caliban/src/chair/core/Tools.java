package chair.core;

/**
 * La boîte à outils pour lejos désespéré
 * 
 * @author nomhad
 */

public class Tools {

	/**
	 * Lejos connaît pas String.compareTo, à moi de refaire ordre
	 * lexicographique en mal. Ne se contente pas d'un -1 ou 1.
	 * Éspérons que ça passe l'Unicode et autres subtilités.
	 * 
	 * Identique à s1.compareTo(s2) d'un meilleur monde
	 * 
	 * @param s1
	 * @param s2
	 */
	public static int compareToDuPauvre(String s1, String s2) {

		// déjà si elles sont identiques c'est 0
		if (s1.equals(s2))
			return 0;

		// La taille de la plus courte chaîne
		int l1 = s1.length();
		int l2 = s2.length();
		int minLength = (l1 < l2) ? l1 : l2;

		// On va parcourir les chaînes, à la première différence on la retourne
		for (int i = 0; i < minLength; i++) {
			char c1 = s1.charAt(i);
			char c2 = s2.charAt(i);
			if (c1 != c2)
				return c1 - c2;
		}
		// Les chaînes sont donc de longeurs différentes : retourne la distance
		return l1 - l2;
	}
}
