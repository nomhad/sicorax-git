package chair.core;

import lejos.nxt.comm.RConsole;

/**
 * 
 * Si tourne sur leJOS envoie logs vers RConsole, sinon vers sortie standard
 * 
 * @author nomhad
 * 
 */

public class Diary {
	private static boolean isLeJOS = isRunningOnLeJOS();
	private static boolean isInit = false;
	private final static String delimiter = "\n----\n";

	/**
	 * Retourne vrai si l'application tourne actuellement sur la brique leJOS
	 * 
	 */
	public static boolean isRunningOnLeJOS() {
		// FIXME: test complètement pourri mais je ne vois que ça pour le moment
		if (Runtime.getRuntime().totalMemory() < 128000)
			return true;
		return false;
	}

	/**
	 * Si sur leJOS ouverture de la communication avec la brique
	 */
	public static void init() {
		if (!isInit) {
			isInit = true;
			if (isLeJOS) {
				// Attend 5 secondes connexion bluetooth puis 5 secondes
				// connexion USB si ça n'a rien donné
				RConsole.openBluetooth(5000);
				if (!RConsole.isOpen())
					RConsole.openUSB(5000);
			}
		}
		log(delimiter + "Cher Journal...\n");
	}

	/**
	 * Bricolage, sélection du log externalisée par la suite
	 * 
	 * @param note
	 *            message to send to log output
	 */
	public static void log(String note) {
		if (isLeJOS)
			RConsole.print(note);
		else
			System.out.print(note);
	}

	/**
	 * Je ne suis pas persuadé que ce soit vraiment nécessaire mais ça m'amuse.
	 * Il faut bien peupler cette classe chétive
	 * 
	 */
	public static void logln(String note) {
		log(note + "\n");
	}

	/**
	 * Il ne faudra pas oublier de fermer la porte en sortant
	 */
	static public void terminate() {
		log("\nEt merci pour le poisson !" + delimiter);
		if (isLeJOS)
			RConsole.close();
	}
}
