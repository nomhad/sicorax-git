package chair.core;

import java.io.Serializable;

public class StimulusPair implements Comparable<StimulusPair>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2317658816218385459L;
	public SimulationStimulus stimulusA;
	public SimulationStimulus stimulusB;

	public StimulusPair() {
		stimulusA = null;
		stimulusB = null;
	}

	public StimulusPair(SimulationStimulus stimA, SimulationStimulus stimB) {
		stimulusA = stimA;
		stimulusB = stimB;
	}

	public boolean sameStimulus() {
		return stimulusA.getStimulus().equals(stimulusB.getStimulus());

	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof StimulusPair) {
			StimulusPair other = (StimulusPair) obj;
			return stimulusA.equals(other.stimulusA)
					&& stimulusB.equals(other.stimulusB);
		}
		return false;
	}

	// Pour un Hashtable au poil soyeux
	// Code basé sur cette page, peut-être à améliorer
	// http://stevenharman.net/blog/archive/2006/04/28/HowTo_Override_equals_and_hashCode_Part2.aspx
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash
				+ (null == this.stimulusA ? 0 : this.stimulusA.hashCode());
		hash = 31 * hash
				+ (null == this.stimulusB ? 0 : this.stimulusB.hashCode());
		return hash;
	}

	public String toString() {
		return stimulusA + " -> " + stimulusB;
	}

	@Override
	public int compareTo(StimulusPair other) {
		int aComparaison = stimulusA.compareTo(other.stimulusA);
		int bComparaison = stimulusB.compareTo(other.stimulusB);

		if (aComparaison != 0)
			return aComparaison;
		else
			return bComparaison;

	}

}
