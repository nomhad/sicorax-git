package chair.core;

import java.io.Serializable;

public class Stimulus implements Comparable<Stimulus>, Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2117345513260892269L;

	public enum StimulusType {
		CS, US
		// TODO définir si l'on met en place les réponses
		// Si oui, doit on ajouter des réponses réflexes (UR) afin d'avoir une
		// possibilité de conditionnement

	};

	protected String name;
	protected StimulusType type;
	// La valence émotionnelle du stimulus. Par défaut neutre, positive si
	// plutôt récompense, négative si plutôt punition. Non utilisé dans core.
	protected int reward = 0;

	public Stimulus(String name, StimulusType type) {
		super();
		this.name = name;
		this.type = type;
	}

	public Stimulus(String name, StimulusType type, int reward) {
		this(name, type);
		this.reward = reward;
	}

	public String getName() {
		return name;
	}

	public StimulusType getType() {
		return type;
	}

	public int getReward() {
		return reward;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Stimulus) {
			Stimulus other = (Stimulus) obj;
			return name.equals(other.name) && type == other.type
					&& reward == other.reward;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 5;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + reward;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public int compareTo(Stimulus o) {

		return Tools.compareToDuPauvre(name, o.name);
	}

	@Override
	public Object clone() {
		Stimulus stim = null;
		try {
			stim = (Stimulus) super.clone();
		} catch (CloneNotSupportedException e) {
		}
		return stim;
	}
}
