package chair.core;

/**
 * 
 * Besoin de pouvoir ajuster écoulement du temps suivant substrat. Pas forcément
 * la plus belle façon de faire
 * 
 */
public class Parameters {

	private static long stepTime = 10;
	private static long deltaMax = 5000;

	public static long getStepTime() {
		return stepTime;
	}

	public static void setStepTime(long stepTime) {
		Parameters.stepTime = stepTime;
	}

	public static long getDeltaMax() {
		return deltaMax;
	}

	public static void setDeltaMax(long deltaMax) {
		Parameters.deltaMax = deltaMax;
	}
}
