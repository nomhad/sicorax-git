package chair.core;

import java.io.Serializable;

public class SimulationStimulus implements Comparable<SimulationStimulus>,
		Serializable {

	private static final long serialVersionUID = -5513909489230337376L;

	protected Stimulus stimulus;
	protected long beginTime;
	protected boolean risingEdge;

	public SimulationStimulus(Stimulus _stimulus, long beginTime,
			boolean risingEdge) {
		if (_stimulus == null) {
			throw new IllegalArgumentException("Stimulus couldn't be null");
		}

		stimulus = _stimulus;
		this.beginTime = beginTime;
		this.risingEdge = risingEdge;

	}

	public boolean equals(Object obj) {
		if (obj.getClass() == SimulationStimulus.class) {
			SimulationStimulus other = (SimulationStimulus) obj;
			return (risingEdge == other.risingEdge && stimulus
					.equals(other.stimulus));
		}
		throw new RuntimeException(
				"Erreur de classe dans le equals de SimulationStimulus");
		// return false;
	}

	// Les perfs sont peut-être pas excellentes...
	@Override
	public int hashCode() {
		// NB: beginTime rentre pas en compte dans égalité
		int hash = 13;
		hash = 23 * hash + Boolean.valueOf(risingEdge).hashCode();
		// hashCode pas redéfini dans Stimulus (bug latent suivant
		// instanciations ?)
		hash = 23 * hash
				+ (null == this.stimulus ? 0 : this.stimulus.hashCode());
		return hash;
	}

	public Stimulus getStimulus() {
		return stimulus;
	}

	public void setStimulus(Stimulus stimulus) {
		this.stimulus = stimulus;
	}

	public long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public boolean isRisingEdge() {
		return risingEdge;
	}

	public void setRisingEdge(boolean risingEdge) {
		this.risingEdge = risingEdge;
	}

	public String toString() {
		return stimulus.getName() + " (" + (risingEdge ? "M" : "D") + ")";
	}

	@Override
	public int compareTo(SimulationStimulus other) {
		if (other == null || stimulus == null) {
			System.out.println("bloup");
		}

		int comparaison = stimulus.compareTo(other.stimulus);
		if (comparaison != 0)
			return comparaison;
		else
			return (risingEdge ? 1 : 0) - (other.risingEdge ? 1 : 0);

	}

}
