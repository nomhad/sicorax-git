package chair;

import java.util.NoSuchElementException;
import java.util.Vector;

/*
 * $Log: Queue.java,v $
 * Revision 1.2  2005/11/23 17:46:45  mpscholz
 * minor javadoc related changes
 *
 * Revision 1.1  2003/08/17 14:59:42  mpscholz
 * enhanced Vector
 * added Stack and Queue and associated exception classes
 *
 */

//import java.util.Vector;

/////////////////////////////////////////////////////////
//Dans le cas où utilise jre va lever pas mal de warnings autrement
@SuppressWarnings("all")
/**
 * Récupéré de svn. Problème si utilise java.util.Queue : sur JRE c'est une
 * interface, bibliothèque leJOS prend pas le pas dans ce cas sur JRE. Remplacé
 * EmptyQueueException qui n'existe pas dans JRE par NoSuchElementException pour
 * ne pas lever java.lang.SecurityException: Prohibited package name: java.util
 * 
 * A FIFO Queue of objects.
 */
public class Queue extends Vector {

	// TODO in JDK, java.util.Queue is an interface

	// //////////////////////////////////////////
	// constants
	// //////////////////////////////////////////

	// //////////////////////////////////////////
	// fields
	// //////////////////////////////////////////

	// //////////////////////////////////////////
	// constructors
	// //////////////////////////////////////////

	// //////////////////////////////////////////
	/**
	 * creates a new Queue instance
	 */
	public Queue() {
		// do nothing
	} // Queue()

	// //////////////////////////////////////////
	/**
	 * pushes an object onto the Queue
	 * 
	 * @param anObject
	 *            the object
	 * @return Object the object pushed onto the Queue
	 */
	public Object push(Object anObject) {
		// add the object to base vector
		addElement(anObject);
		return anObject;
	} // push()

	// //////////////////////////////////////////
	/**
	 * fetches an object from the start of the Queue and removes it
	 * 
	 * @return Object the object removed from the start of the stock
	 * @throws EmptyQueueException
	 */
	public synchronized Object pop() throws NoSuchElementException {
		// get object
		Object popped = peek();
		// remove and return object
		removeElementAt(0);
		return popped;
	} // pop()

	// //////////////////////////////////////////
	/**
	 * fetches an object from the start of the Queue <br>
	 * does not remove it!
	 * 
	 * @return Object the object at the start of the Queue
	 * @throws EmptyQueueException
	 */
	public synchronized Object peek() throws NoSuchElementException {
		// empty Queue?
		if (size() == 0)
			throw new NoSuchElementException();
		// return first element
		return elementAt(0);
	} // peek()

	// //////////////////////////////////////////
	/**
	 * is this Queue empty?
	 * 
	 * @return boolean true, if the Queue is empty
	 */
	public boolean empty() {
		return (size() == 0);
	} // empty()

} // class Queue