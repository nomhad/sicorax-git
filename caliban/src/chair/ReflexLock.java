package chair;

/**
 * Classe fantoche utilisé par organ pour verrouiller fonctions.
 * 
 * @author nomhad
 * 
 */
class ReflexLock extends Reflex {

	/**
	 * 
	 * @param fct
	 *            fonction à vérouiller
	 * @param name
	 *            le nom de l'organe appelant pour information, le nom du
	 *            réflexe sera complété du nom de la fonction
	 */
	ReflexLock(Function fct, String name) {
		super(fct, name + " ReflexLock " + fct.getName(), ReflexType.LOCK);
	}

	/**
	 * Tout l'art de ce réflexe est de ne rien faire
	 */
	@Override
	protected void run(Impulse impulse) {
	}

}
