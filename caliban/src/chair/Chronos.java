package chair;

import chair.core.Parameters;

/**
 * Gestion du temps : celui du système et les différentes fenêtre temporelles
 * mises en jeu dans l'XP.
 * 
 * @author nomhad
 * 
 */

final public class Chronos {
	/**
	 * Combien de ms le thead du thalamus doit attendre entre deux sondage
	 * auprès des organes ?
	 */
	private static long THALAMUS_POLLING_TIME = 10;
	/** Idem avec Striatum et interrogation des fonctions */
	private static long STRIATUM_POLLING_TIME = 10;
	/**
	 * Combien de temps les signaux émis par les organes doivent-ils être
	 * stables afin de constituer un contexte pertinent. Utilisé dans Thalamus.
	 */
	private static long CONTEXT_WINDOW_TIME = 10000;
	/**
	 * Avant combien de temps Function doit-il essayer un nouveau comportement ?
	 */
	private static long BEHAVIOR_POLLING_TIME = 7000;

	/** Notre compteur partira de 0 */
	private static final long START_TIME = topSystem();

	/**
	 * Combien de temps la boucle principale doit attendre entre chaque
	 * itération tour d'horloge de Brain (et par extension, du modèle). Enrobe
	 * core.Parameters.
	 */
	public static long getModelStepTime() {
		return Parameters.getStepTime();
	}

	/** Utilise en interne Parameters.setStepTime() */
	public static void setModelStepTime(long stepTime) {
		Parameters.setStepTime(stepTime);
	}

	/**
	 * La fenêtre temporelle pendant laquelle le modèle de conditionnement peut
	 * associer deux stimuli. Enrobe core.Parameters.
	 */
	public static long getModelDeltaMax() {
		return Parameters.getDeltaMax();
	}

	/** Utilise en interne Parameters.setDeltaMax() */
	public static void setModelDeltaMax(long stepTime) {
		Parameters.setDeltaMax(stepTime);
	}

	/**
	 * @return THALAMUS_POLLING_TIME
	 */
	protected static long getThalamusPollingTime() {
		return THALAMUS_POLLING_TIME;
	}

	/**
	 * @param time
	 *            THALAMUS_POLLING_TIME to set
	 */
	protected static void setThalamusPollingTime(long time) {
		THALAMUS_POLLING_TIME = time;
	}

	/**
	 * @return STRIATUM_POLLING_TIME
	 */
	protected static long getSriatumPollingTime() {
		return STRIATUM_POLLING_TIME;
	}

	/**
	 * @param time
	 *            STRIATUM_POLLING_TIME to set
	 */
	protected static void setSriatumPollingTime(long time) {
		STRIATUM_POLLING_TIME = time;
	}

	/**
	 * @return CONTEXT_WINDOW_TIME
	 */
	protected static long getContextWindowTime() {
		return CONTEXT_WINDOW_TIME;
	}

	/**
	 * @param time
	 *            CONTEXT_WINDOW_TIME to set
	 */
	protected static void setContextWindowTime(long time) {
		CONTEXT_WINDOW_TIME = time;
	}

	/**
	 * @return BEHAVIOR_POLLING_TIME
	 */
	protected static long getBehaviorPollingTime() {
		return BEHAVIOR_POLLING_TIME;
	}

	/**
	 * @param time
	 *            BEHAVIOR_POLLING_TIME to set
	 */
	public static void setBehaviorPollingTime(long time) {
		BEHAVIOR_POLLING_TIME = time;
	}

	/**
	 * Quel temps est-il depuis l'initialisation de Chronos ?
	 * 
	 * @return temps actuel depuis lancement du programme en ms
	 */
	public static long top() {
		return System.currentTimeMillis() - START_TIME;
	}

	/**
	 * Le temps du point de vue du système
	 * 
	 * @return temps actuel du système en ms
	 */
	private static long topSystem() {
		return System.currentTimeMillis();
	}
}
