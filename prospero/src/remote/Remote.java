package remote;

import java.util.Scanner;

import robot.Head;
import robot.Substance;

/**
 * C'est ce programme qu'il faut faire tourner sur le PC. On pourra y
 * sélectionner le robot qu'on veut faire tourner.
 * 
 * @author nomhad
 * 
 */
public class Remote {

	public static void main(String[] args) {

		// Les noms des robots tels qu'ils apparaîtront pour l'utilisateur
		String[] robotsName = { "Pavlov", "Skinner", "Context", "Calibeta",
				"Calibeta Simple", "Calicon Simple" };
		// La classe fille de Head corresondante pour instanciation
		String[] robotsClass = { "robot.pavlov.HeadPavlov",
				"robot.skinner.HeadSkinner", "robot.context.HeadContext",
				"robot.calibeta.HeadCalibeta", "robot.calibeta.HeadCalibetaS",
				"robot.calicon.HeadCaliconS" };

		System.out.println("Les robots à votre disposition : ");

		for (int i = 0; i < robotsName.length; i++)
			System.out.println((i + 1) + " : " + robotsName[i]);

		System.out.print("\nVotre choix > ");
		Scanner sc = new Scanner(System.in);
		// Pour l'utilisateur commence à 1
		int i = sc.nextInt() - 1;
		// Si le choix n'est pas valide, on quitte
		if (i < 0 || i > robotsName.length) {
			System.out.println("Robot absent pour le moment.");
			System.exit(1);
		}

		// On demande à l'utilisateur si veut debug
		System.out
				.println("\nSouhaitez-vous afficher des informations de debug étendues ?");
		System.out.print("o/N ? > ");
		boolean debug = sc.next().equals("o");
		if (debug)
			System.out.println("Debug sélectionné");
		else
			System.out.println("Debug NON sélectionné");

		System.out.print("\nVous avez choisi " + robotsName[i]
				+ ", sélection de " + robotsClass[i] + "...");

		// Récupère la classe correspondante
		Class<?> candidate = null;
		try {
			candidate = Class.forName(robotsClass[i]);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}

		// Instancie la classe (il y aura connexion au NXT du fait de
		// Diary.init())
		Head teteu = null;
		if (candidate != null)
			try {
				System.out.println("instanciation...\n");
				teteu = (Head) candidate.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		if (teteu != null) {
			System.out.println("Lancement de la simulation");
			new Substance(teteu.getBrain(), false, debug);
		}

	}
}
